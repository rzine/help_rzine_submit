# Soumettre une fiche Rzine [<img src="https://rzine.fr/img/Rzine_logo.png"  align="right" width="120"/>](https://rzine.fr/)
### Processus de soumission d’une fiche au comité de lecture
**Comité éditorial Rzine**
<br/>  
-> [**Consulter la documentation**](https://rzine.gitpages.huma-num.fr/help_rzine_submit)

<br/>  

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/4.0/)
